﻿namespace CapDatGen
{
    partial class frmCapDatGetMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRfrshData = new System.Windows.Forms.Button();
            this.lblFormMain = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtRfrshSts = new System.Windows.Forms.TextBox();
            this.lblRfrshSts = new System.Windows.Forms.Label();
            this.btnShftUpd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRfrshData
            // 
            this.btnRfrshData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRfrshData.Location = new System.Drawing.Point(112, 94);
            this.btnRfrshData.Name = "btnRfrshData";
            this.btnRfrshData.Size = new System.Drawing.Size(117, 45);
            this.btnRfrshData.TabIndex = 0;
            this.btnRfrshData.Text = "Refresh Labor Report Data";
            this.btnRfrshData.UseVisualStyleBackColor = true;
            this.btnRfrshData.Click += new System.EventHandler(this.btnRfrshData_Click);
            // 
            // lblFormMain
            // 
            this.lblFormMain.AutoSize = true;
            this.lblFormMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFormMain.Location = new System.Drawing.Point(89, 32);
            this.lblFormMain.Name = "lblFormMain";
            this.lblFormMain.Size = new System.Drawing.Size(316, 20);
            this.lblFormMain.TabIndex = 1;
            this.lblFormMain.Text = "Load Current Capacity Reporting Data";
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(375, 207);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtRfrshSts
            // 
            this.txtRfrshSts.Location = new System.Drawing.Point(305, 108);
            this.txtRfrshSts.Name = "txtRfrshSts";
            this.txtRfrshSts.Size = new System.Drawing.Size(145, 20);
            this.txtRfrshSts.TabIndex = 5;
            // 
            // lblRfrshSts
            // 
            this.lblRfrshSts.AutoSize = true;
            this.lblRfrshSts.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRfrshSts.Location = new System.Drawing.Point(358, 90);
            this.lblRfrshSts.Name = "lblRfrshSts";
            this.lblRfrshSts.Size = new System.Drawing.Size(47, 15);
            this.lblRfrshSts.TabIndex = 6;
            this.lblRfrshSts.Text = "Status";
            // 
            // btnShftUpd
            // 
            this.btnShftUpd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShftUpd.Location = new System.Drawing.Point(93, 186);
            this.btnShftUpd.Name = "btnShftUpd";
            this.btnShftUpd.Size = new System.Drawing.Size(136, 44);
            this.btnShftUpd.TabIndex = 7;
            this.btnShftUpd.Text = "Update Workday and Labor Hours";
            this.btnShftUpd.UseVisualStyleBackColor = true;
            this.btnShftUpd.Click += new System.EventHandler(this.btnShftUpd_Click);
            // 
            // frmCapDatGetMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 262);
            this.Controls.Add(this.btnShftUpd);
            this.Controls.Add(this.lblRfrshSts);
            this.Controls.Add(this.txtRfrshSts);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblFormMain);
            this.Controls.Add(this.btnRfrshData);
            this.Name = "frmCapDatGetMain";
            this.Text = "Generate Capacity Report Detail Data";
            this.Load += new System.EventHandler(this.frmCapDatGetMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRfrshData;
        private System.Windows.Forms.Label lblFormMain;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtRfrshSts;
        private System.Windows.Forms.Label lblRfrshSts;
        private System.Windows.Forms.Button btnShftUpd;
    }
}

