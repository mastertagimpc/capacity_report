﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CapDatGen
{
    public partial class frmSetShiftParams : Form
    {
        private string connStr;         // DB connection string

        public frmSetShiftParams()
        {
            InitializeComponent();
        }

        public string DBConnStr
        {
            set
            {
                connStr = value;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpd_Click(object sender, EventArgs e)
        {
            double convLabHrs;
            string wrkDatMode;
            string selBldg;

            // Validate entered fields
            if (dateTimePicker1.Value.Date > dateTimePicker2.Value.Date)
            {
                MessageBox.Show("End date must be after start date");
                dateTimePicker1.Focus();
                return;
            }

            try
            {
                selBldg = cmbBldg.SelectedItem.ToString();
            }
            catch
            {
                MessageBox.Show("Please select a building");
                cmbBldg.Focus();
                return;
            }


            try
            {
                convLabHrs = Convert.ToDouble(txtLabHrs.Text);
            }
            catch
            {
                MessageBox.Show("Please enter converting labor hours");
                txtLabHrs.Focus();
                return;
            }

            if (convLabHrs <= 0)
            {
                MessageBox.Show("Labor hours must be greater than zero");
                txtLabHrs.Focus();
                return;
            }

            // Date mode
            if (cbWrkDysOny.Checked == true)
            {
                wrkDatMode = "W";
            }
            else
            {
                wrkDatMode = "A";
            }

            // Call procedure to update the calendar
            DataAccess objDa;

            objDa = new DataAccess(connStr);

            objDa.updWrkDayCal(selBldg, dateTimePicker1.Value.Date + dateTimePicker3.Value.TimeOfDay,
                                            dateTimePicker2.Value.Date + dateTimePicker4.Value.TimeOfDay,
                                            convLabHrs, wrkDatMode);

            return;

        }
    }
}
