﻿namespace CapDatGen
{
    partial class frmSetShiftParams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.lblShiftHdr = new System.Windows.Forms.Label();
            this.lblLocn = new System.Windows.Forms.Label();
            this.lblDates = new System.Windows.Forms.Label();
            this.lblDat1 = new System.Windows.Forms.Label();
            this.lblDat2 = new System.Windows.Forms.Label();
            this.lblShiftTim = new System.Windows.Forms.Label();
            this.lblLabHrs = new System.Windows.Forms.Label();
            this.lblTim1 = new System.Windows.Forms.Label();
            this.lblTim2 = new System.Windows.Forms.Label();
            this.txtLabHrs = new System.Windows.Forms.TextBox();
            this.btnUpd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cmbBldg = new System.Windows.Forms.ComboBox();
            this.cbWrkDysOny = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(87, 234);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(265, 21);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(87, 291);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(265, 21);
            this.dateTimePicker2.TabIndex = 2;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker3.Location = new System.Drawing.Point(481, 234);
            this.dateTimePicker3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.ShowUpDown = true;
            this.dateTimePicker3.Size = new System.Drawing.Size(265, 21);
            this.dateTimePicker3.TabIndex = 3;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker4.Location = new System.Drawing.Point(481, 295);
            this.dateTimePicker4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.ShowUpDown = true;
            this.dateTimePicker4.Size = new System.Drawing.Size(265, 21);
            this.dateTimePicker4.TabIndex = 4;
            // 
            // lblShiftHdr
            // 
            this.lblShiftHdr.AutoSize = true;
            this.lblShiftHdr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftHdr.Location = new System.Drawing.Point(199, 45);
            this.lblShiftHdr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiftHdr.Name = "lblShiftHdr";
            this.lblShiftHdr.Size = new System.Drawing.Size(281, 20);
            this.lblShiftHdr.TabIndex = 5;
            this.lblShiftHdr.Text = "Shift and Labor Pool Maintenance";
            // 
            // lblLocn
            // 
            this.lblLocn.AutoSize = true;
            this.lblLocn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocn.Location = new System.Drawing.Point(67, 133);
            this.lblLocn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLocn.Name = "lblLocn";
            this.lblLocn.Size = new System.Drawing.Size(60, 15);
            this.lblLocn.TabIndex = 6;
            this.lblLocn.Text = "Building";
            // 
            // lblDates
            // 
            this.lblDates.AutoSize = true;
            this.lblDates.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDates.Location = new System.Drawing.Point(124, 198);
            this.lblDates.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDates.Name = "lblDates";
            this.lblDates.Size = new System.Drawing.Size(83, 15);
            this.lblDates.TabIndex = 7;
            this.lblDates.Text = "Date Range";
            // 
            // lblDat1
            // 
            this.lblDat1.AutoSize = true;
            this.lblDat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDat1.Location = new System.Drawing.Point(25, 234);
            this.lblDat1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDat1.Name = "lblDat1";
            this.lblDat1.Size = new System.Drawing.Size(40, 15);
            this.lblDat1.TabIndex = 8;
            this.lblDat1.Text = "From";
            // 
            // lblDat2
            // 
            this.lblDat2.AutoSize = true;
            this.lblDat2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDat2.Location = new System.Drawing.Point(48, 295);
            this.lblDat2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDat2.Name = "lblDat2";
            this.lblDat2.Size = new System.Drawing.Size(23, 15);
            this.lblDat2.TabIndex = 9;
            this.lblDat2.Text = "To";
            // 
            // lblShiftTim
            // 
            this.lblShiftTim.AutoSize = true;
            this.lblShiftTim.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShiftTim.Location = new System.Drawing.Point(553, 198);
            this.lblShiftTim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblShiftTim.Name = "lblShiftTim";
            this.lblShiftTim.Size = new System.Drawing.Size(72, 15);
            this.lblShiftTim.TabIndex = 10;
            this.lblShiftTim.Text = "Shift Time";
            // 
            // lblLabHrs
            // 
            this.lblLabHrs.AutoSize = true;
            this.lblLabHrs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabHrs.Location = new System.Drawing.Point(25, 375);
            this.lblLabHrs.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLabHrs.Name = "lblLabHrs";
            this.lblLabHrs.Size = new System.Drawing.Size(158, 15);
            this.lblLabHrs.TabIndex = 11;
            this.lblLabHrs.Text = "Converting Labor Hours";
            // 
            // lblTim1
            // 
            this.lblTim1.AutoSize = true;
            this.lblTim1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTim1.Location = new System.Drawing.Point(387, 239);
            this.lblTim1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTim1.Name = "lblTim1";
            this.lblTim1.Size = new System.Drawing.Size(65, 15);
            this.lblTim1.TabIndex = 12;
            this.lblTim1.Text = "Day Start";
            // 
            // lblTim2
            // 
            this.lblTim2.AutoSize = true;
            this.lblTim2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTim2.Location = new System.Drawing.Point(393, 295);
            this.lblTim2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTim2.Name = "lblTim2";
            this.lblTim2.Size = new System.Drawing.Size(60, 15);
            this.lblTim2.TabIndex = 13;
            this.lblTim2.Text = "Day End";
            // 
            // txtLabHrs
            // 
            this.txtLabHrs.Location = new System.Drawing.Point(259, 374);
            this.txtLabHrs.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.txtLabHrs.Name = "txtLabHrs";
            this.txtLabHrs.Size = new System.Drawing.Size(132, 21);
            this.txtLabHrs.TabIndex = 14;
            // 
            // btnUpd
            // 
            this.btnUpd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpd.Location = new System.Drawing.Point(585, 372);
            this.btnUpd.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnUpd.Name = "btnUpd";
            this.btnUpd.Size = new System.Drawing.Size(100, 27);
            this.btnUpd.TabIndex = 15;
            this.btnUpd.Text = "Apply";
            this.btnUpd.UseVisualStyleBackColor = true;
            this.btnUpd.Click += new System.EventHandler(this.btnUpd_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(585, 436);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(100, 27);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cmbBldg
            // 
            this.cmbBldg.FormattingEnabled = true;
            this.cmbBldg.Items.AddRange(new object[] {
            "WALSH",
            "FC"});
            this.cmbBldg.Location = new System.Drawing.Point(155, 132);
            this.cmbBldg.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cmbBldg.Name = "cmbBldg";
            this.cmbBldg.Size = new System.Drawing.Size(160, 23);
            this.cmbBldg.TabIndex = 17;
            // 
            // cbWrkDysOny
            // 
            this.cbWrkDysOny.AutoSize = true;
            this.cbWrkDysOny.Checked = true;
            this.cbWrkDysOny.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbWrkDysOny.Location = new System.Drawing.Point(556, 133);
            this.cbWrkDysOny.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbWrkDysOny.Name = "cbWrkDysOny";
            this.cbWrkDysOny.Size = new System.Drawing.Size(170, 19);
            this.cbWrkDysOny.TabIndex = 19;
            this.cbWrkDysOny.Text = "Normal Workdays Only";
            this.cbWrkDysOny.UseVisualStyleBackColor = true;
            // 
            // frmSetShiftParams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 489);
            this.Controls.Add(this.cbWrkDysOny);
            this.Controls.Add(this.cmbBldg);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnUpd);
            this.Controls.Add(this.txtLabHrs);
            this.Controls.Add(this.lblTim2);
            this.Controls.Add(this.lblTim1);
            this.Controls.Add(this.lblLabHrs);
            this.Controls.Add(this.lblShiftTim);
            this.Controls.Add(this.lblDat2);
            this.Controls.Add(this.lblDat1);
            this.Controls.Add(this.lblDates);
            this.Controls.Add(this.lblLocn);
            this.Controls.Add(this.lblShiftHdr);
            this.Controls.Add(this.dateTimePicker4);
            this.Controls.Add(this.dateTimePicker3);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "frmSetShiftParams";
            this.Text = "Set Shift Parameters";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label lblShiftHdr;
        private System.Windows.Forms.Label lblLocn;
        private System.Windows.Forms.Label lblDates;
        private System.Windows.Forms.Label lblDat1;
        private System.Windows.Forms.Label lblDat2;
        private System.Windows.Forms.Label lblShiftTim;
        private System.Windows.Forms.Label lblLabHrs;
        private System.Windows.Forms.Label lblTim1;
        private System.Windows.Forms.Label lblTim2;
        private System.Windows.Forms.TextBox txtLabHrs;
        private System.Windows.Forms.Button btnUpd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox cmbBldg;
        private System.Windows.Forms.CheckBox cbWrkDysOny;
    }
}