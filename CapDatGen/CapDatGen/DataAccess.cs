﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace CapDatGen
{
    class DataAccess
    {
        private OracleConnection objConn;


        // Constructor
        public DataAccess(string strConnString)
        {
            try
            {
                objConn = new OracleConnection();
                objConn.ConnectionString = strConnString;
            }
            catch (OracleException err)
            {
                MessageBox.Show("Error establishing Oracle connection: " + err.Message.ToString());
            }
        }
        // Destructor
        ~DataAccess()
        {
            if (objConn != null)
            {
                if (objConn.State == ConnectionState.Open)
                {
                    objConn.Close();
                }
            }
        }

        // Clear out PSA task table
        public void ClrPsaTskTab()
        {
            OracleCommand cmd = new OracleCommand("mtag_efir_cap_rep_utls.clr_psa_task_det", objConn);
            cmd.CommandType = CommandType.StoredProcedure;

            OracleDataAdapter da = new OracleDataAdapter(cmd);
            OracleCommandBuilder cb = new OracleCommandBuilder(da);

            try
            {
                objConn.Open();
                cmd.ExecuteNonQuery();
                objConn.Close();
            }
            catch (OracleException err)
            {
                MessageBox.Show("Error clearing PSA task data: " + err.Message.ToString());
            }
        }

        // Build Capacity Reporting Table
        public string BldCapRptDat(out string stsMsg)
        {
            OracleCommand cmd = new OracleCommand("mtag_efir_cap_rep_utls.bld_cap_rpt_dat", objConn);
            cmd.CommandType = CommandType.StoredProcedure;

            OracleParameter pSts = new OracleParameter();
            pSts.ParameterName = "p_sts";
            pSts.OracleDbType = OracleDbType.Varchar2;
            pSts.Direction = ParameterDirection.Output;
            pSts.Size = 20;
            cmd.Parameters.Add(pSts);

            OracleParameter pStsMsg = new OracleParameter();
            pStsMsg.ParameterName = "p_sts_msg";
            pStsMsg.OracleDbType = OracleDbType.Varchar2;
            pStsMsg.Direction = ParameterDirection.Output;
            pStsMsg.Size = 100;
            cmd.Parameters.Add(pStsMsg);

            OracleDataAdapter da = new OracleDataAdapter(cmd);
            OracleCommandBuilder cb = new OracleCommandBuilder(da);

            try
            {
                objConn.Open();
                cmd.ExecuteNonQuery();
                objConn.Close();
            }
            catch (OracleException err)
            {
                MessageBox.Show("bld_cap_rpt_dat error: " + err.Message.ToString());
            }

            stsMsg = pStsMsg.Value.ToString();

            return pSts.Value.ToString();

        }

        // Update workday shift times
        public void updWrkDayCal(string bldg, DateTime stDate, DateTime endDate, double convLabHrs, string datMode)
        {
            OracleCommand cmd = new OracleCommand("apps.mtag_efir_cap_rep_utls.upd_wrkday_cal", objConn);
            cmd.CommandType = CommandType.StoredProcedure;

            OracleParameter pBldg = new OracleParameter();
            pBldg.ParameterName = "p_bldg";
            pBldg.OracleDbType = OracleDbType.Varchar2;
            pBldg.Direction = ParameterDirection.Input;
            pBldg.Value = bldg;
            cmd.Parameters.Add(pBldg);

            OracleParameter pStDate = new OracleParameter();
            pStDate.ParameterName = "p_st_date";
            pStDate.OracleDbType = OracleDbType.Date;
            pStDate.Direction = ParameterDirection.Input;
            pStDate.Value = stDate;
            cmd.Parameters.Add(pStDate);

            OracleParameter pEndDate = new OracleParameter();
            pEndDate.ParameterName = "p_end_date";
            pEndDate.OracleDbType = OracleDbType.Date;
            pEndDate.Direction = ParameterDirection.Input;
            pEndDate.Value = endDate;
            cmd.Parameters.Add(pEndDate);

            OracleParameter pConvLabHrs = new OracleParameter();
            pConvLabHrs.ParameterName = "p_conv_lab_hrs";
            pConvLabHrs.OracleDbType = OracleDbType.Double;
            pConvLabHrs.Direction = ParameterDirection.Input;
            pConvLabHrs.Value = convLabHrs;
            cmd.Parameters.Add(pConvLabHrs);

            OracleParameter pDatMode = new OracleParameter();
            pDatMode.ParameterName = "p_dat_mode";
            pDatMode.OracleDbType = OracleDbType.Varchar2;
            pDatMode.Direction = ParameterDirection.Input;
            pDatMode.Value = datMode;
            cmd.Parameters.Add(pDatMode);

            OracleDataAdapter da = new OracleDataAdapter(cmd);
            OracleCommandBuilder cb = new OracleCommandBuilder(da);

            try
            {
                objConn.Open();
                cmd.ExecuteNonQuery();
                objConn.Close();
            }
            catch (OracleException err)
            {
                MessageBox.Show(err.Message.ToString());
            }

        }

        // Load PSA Task Detail
        public string LodPSATaskDet()
        {
            double dtWrk;

            // Set up Orcle command and parameters

            OracleCommand cmd = new OracleCommand("apps.mtag_efir_cap_rep_utls.lod_psa_task_dat", objConn);
            cmd.CommandType = CommandType.StoredProcedure;

            // Define parameters
            // values will be added in SQL read loop

            OracleParameter pjob = new OracleParameter();
            pjob.ParameterName = "p_job";
            pjob.OracleDbType = OracleDbType.Varchar2;
            pjob.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pjob);

            OracleParameter pwc = new OracleParameter();
            pwc.ParameterName = "p_wc";
            pwc.OracleDbType = OracleDbType.Varchar2;
            pwc.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pwc);

            OracleParameter psttime = new OracleParameter();
            psttime.ParameterName = "p_st_time";
            psttime.OracleDbType = OracleDbType.Date;
            psttime.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(psttime);

            OracleParameter pendtime = new OracleParameter();
            pendtime.ParameterName = "p_end_time";
            pendtime.OracleDbType = OracleDbType.Date;
            pendtime.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pendtime);

            OracleParameter pmrmin = new OracleParameter();
            pmrmin.ParameterName = "p_mr_min";
            pmrmin.OracleDbType = OracleDbType.Double;
            pmrmin.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pmrmin);

            OracleParameter prunmin = new OracleParameter();
            prunmin.ParameterName = "p_run_min";
            prunmin.OracleDbType = OracleDbType.Double;
            prunmin.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(prunmin);

            OracleParameter pcompnum = new OracleParameter();
            pcompnum.ParameterName = "p_comp_num";
            pcompnum.OracleDbType = OracleDbType.Double;
            pcompnum.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pcompnum);

            OracleParameter ptasknum = new OracleParameter();
            ptasknum.ParameterName = "p_task_num";
            ptasknum.OracleDbType = OracleDbType.Double;
            ptasknum.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ptasknum);

            OracleParameter ppsarsrc = new OracleParameter();
            ppsarsrc.ParameterName = "p_psa_rsrc";
            ppsarsrc.OracleDbType = OracleDbType.Varchar2;
            ppsarsrc.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ppsarsrc);

            OracleParameter ppsasts = new OracleParameter();
            ppsasts.ParameterName = "p_psa_sts";
            ppsasts.OracleDbType = OracleDbType.Varchar2;
            ppsasts.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ppsasts);

            OracleParameter pqty = new OracleParameter();
            pqty.ParameterName = "p_qty";
            pqty.OracleDbType = OracleDbType.Double;
            pqty.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pqty);

            OracleParameter ppsastrtime = new OracleParameter();
            ppsastrtime.ParameterName = "p_psa_strtime";
            ppsastrtime.OracleDbType = OracleDbType.Double;
            ppsastrtime.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ppsastrtime);

            OracleParameter ppsaedtime = new OracleParameter();
            ppsaedtime.ParameterName = "p_psa_edtime";
            ppsaedtime.OracleDbType = OracleDbType.Double;
            ppsaedtime.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ppsaedtime);

            OracleParameter pptaskstartdt = new OracleParameter();
            pptaskstartdt.ParameterName = "p_ptaskstartdt";
            pptaskstartdt.OracleDbType = OracleDbType.Date;
            pptaskstartdt.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pptaskstartdt);

            OracleParameter pptaskenddt = new OracleParameter();
            pptaskenddt.ParameterName = "p_ptaskenddt";
            pptaskenddt.OracleDbType = OracleDbType.Date;
            pptaskenddt.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pptaskenddt);

            OracleParameter prawmatl = new OracleParameter();
            prawmatl.ParameterName = "p_rawmatl";
            prawmatl.OracleDbType = OracleDbType.Varchar2;
            prawmatl.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(prawmatl);

            OracleParameter pprodgroup = new OracleParameter();
            pprodgroup.ParameterName = "p_prod_group";
            pprodgroup.OracleDbType = OracleDbType.Varchar2;
            pprodgroup.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pprodgroup);

            OracleParameter pjobtype = new OracleParameter();
            pjobtype.ParameterName = "p_job_type";
            pjobtype.OracleDbType = OracleDbType.Varchar2;
            pjobtype.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(pjobtype);

            OracleParameter ptimerequired = new OracleParameter();
            ptimerequired.ParameterName = "p_time_required";
            ptimerequired.OracleDbType = OracleDbType.Varchar2;
            ptimerequired.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ptimerequired);

            OracleParameter ppsanotes = new OracleParameter();
            ppsanotes.ParameterName = "p_psa_notes";
            ppsanotes.OracleDbType = OracleDbType.Varchar2;
            ppsanotes.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(ppsanotes);

            OracleDataAdapter da = new OracleDataAdapter(cmd);
            OracleCommandBuilder cb = new OracleCommandBuilder(da);

            // Open Oracle connection
            try
            {
                objConn.Open();
            }
            catch (OracleException err)
            {
                MessageBox.Show("Error opening Oracle connection: " + err.Message.ToString());
            }

            // Extract job data from PSA

            // Set up SQL Connection

            //SqlConnection sqlConn = new SqlConnection("Data Source=mt-vspsdb\\printstreamdb;Initial Catalog=PSA_Live;Integrated Security=True");
            //update the connection information for Radius 8.1
            SqlConnection sqlConn = new SqlConnection();
            SqlConnectionStringBuilder connectionBuilder = new SqlConnectionStringBuilder();
            connectionBuilder.UserID = "PrintStream";
            connectionBuilder.Password = "Print$tream";
            connectionBuilder.InitialCatalog = "PSA_Live";
            connectionBuilder.DataSource = "mt-psdb\\PRINTSTREAMDB";
            connectionBuilder.ConnectTimeout = 30;
            sqlConn.ConnectionString = connectionBuilder.ConnectionString;

            // Define query command to pull in Job tasks from PSA Job table

            SqlCommand sqlCmd = new SqlCommand("SELECT job.compnum, job.ordernumber, job.job, job.description, job.runlength, job.mrlength," +
                                                    " job.resource, job.status, job.quantity, job.strtime, job.edtime," +
                                                    " job.prevtask, job.nexttask, job.ptaskstartdt, job.ptaskenddt, job.ataskstartdt, " +
                                                    " job.ataskenddt, job.runstatus, job.tech4, job.tech7, job.tech5, ordheader.type, ordheader.txt " +
                                                    "FROM DBO.job job " +
                                                    "INNER JOIN dbo.ordheader ordheader ON job.ordernumber = ordheader.ordernumber" +
                                                    " WHERE job.description != ''" +
                                                    " AND job.status != 'C'", sqlConn);

            // Open the connection and start reading records
            try
            {
                sqlConn.Open();

                // Define a SQL data reader
                SqlDataReader reader = sqlCmd.ExecuteReader();

                while (reader.Read())
                {
                    //  MessageBox.Show(reader["ordernumber"].ToString());

                    // Load parameters for Oracle procedure call
                    // Job number
                    pjob.Value = reader["ordernumber"].ToString();

                    // Workcenter
                    pwc.Value = reader["description"].ToString();

                    // Start date and time
                    dtWrk = (Convert.ToDouble(reader["strtime"].ToString()) / 1440) + 34700;
                    psttime.Value = DateTime.FromOADate(dtWrk);

                    // End date and time
                    dtWrk = (Convert.ToDouble(reader["edtime"].ToString()) / 1440) + 34700;
                    pendtime.Value = DateTime.FromOADate(dtWrk);

                    // Makeready minutes
                    pmrmin.Value = Convert.ToDouble(reader["mrlength"].ToString());

                    // Run minutes
                    prunmin.Value = Convert.ToDouble(reader["runlength"].ToString());

                    // Company number
                    pcompnum.Value = Convert.ToDouble(reader["compnum"].ToString());

                    // Task number
                    ptasknum.Value = Convert.ToDouble(reader["job"].ToString());

                    // Resource
                    ppsarsrc.Value = reader["resource"].ToString();

                    // PSA Status
                    ppsasts.Value = reader["status"].ToString();

                    // PSA quantity
                    pqty.Value = Convert.ToDouble(reader["quantity"].ToString());

                    // PSA starttime - number
                    ppsastrtime.Value = Convert.ToDouble(reader["strtime"].ToString());

                    // PSA endtime - number
                    ppsaedtime.Value = Convert.ToDouble(reader["edtime"].ToString());

                    // Task start date - date
                    if (reader["ptaskstartdt"].ToString().Length > 5)
                    {
                        pptaskstartdt.Value = Convert.ToDateTime(reader["ptaskstartdt"]);
                    }
                    else
                    {
                        pptaskstartdt.Value = DateTime.Now;
                    }


                    // Task end date - date
                    if (reader["ptaskenddt"].ToString().Length > 5)
                    {
                        pptaskenddt.Value = Convert.ToDateTime(reader["ptaskenddt"]);
                    }
                    else
                    {
                        pptaskenddt.Value = DateTime.Now;
                    }


                    // Raw material
                    prawmatl.Value = reader["tech4"].ToString();

                    // Product group
                    pprodgroup.Value = reader["tech7"].ToString();

                    // Job Type
                    pjobtype.Value = reader["type"].ToString();

                    // Time Required
                    ptimerequired.Value = reader["tech5"].ToString();

                    // PSA Notes
                    ppsanotes.Value = reader["txt"].ToString();

                    // Call the procedure
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (OracleException err)
                    {
                        MessageBox.Show(err.Message.ToString());
                    }
                }
                reader.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlConn.Close();
                objConn.Close();
            }

            return "S";

        }

        
                
        
    }
}
