﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using Oracle.DataAccess;

namespace CapDatGen
{
    public partial class frmCapDatGetMain : Form
    {
        private string connStr;

        public frmCapDatGetMain()
        {
            InitializeComponent();
        }

        private void frmCapDatGetMain_Load(object sender, EventArgs e)
        {
            // Set connection string
            //  connStr = "User ID=apps;Password=apps;Data Source=PROD;";

            connStr = "Data Source = (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = dionysus.hq.mastertag.com)(PORT = 1522))(CONNECT_DATA = (SERVICE_NAME = PROD)));" +
                        "User Id = apps; Password = apps;";

            //connStr = "Data Source = (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = dylan.hq.mastertag.com)(PORT = 1524))(CONNECT_DATA = (SERVICE_NAME = TST)));" +
            //            "User Id = apps; Password = apps;";





        }

        private void btnRfrshData_Click(object sender, EventArgs e)
        {

            // Clear out PSA task detail
            DataAccess objDa;

            objDa = new DataAccess(connStr);
            objDa.ClrPsaTskTab();

            objDa.LodPSATaskDet();

            // Build job task detail
            string bldSts;
            string bldStsMsg;

            bldSts = objDa.BldCapRptDat(out bldStsMsg);

            if (bldSts == "FAIL")
            {
                MessageBox.Show(bldStsMsg.ToString());

                txtRfrshSts.Text = "Refresh Failed";
            }
            else
            {
                txtRfrshSts.Text = "Refresh Successful";
            }
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            // Close the form
            this.Close();
        }

        private void btnShftUpd_Click(object sender, EventArgs e)
        {
            frmSetShiftParams frmSetShiftParams = new frmSetShiftParams();

            frmSetShiftParams.DBConnStr = connStr;

            // Show the form
            frmSetShiftParams.ShowDialog(this);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
