CREATE OR REPLACE package      mtag_efir_cap_rep_utls AS

                            
PROCEDURE lod_psa_task_dat(p_job IN VARCHAR2,
                            p_wc IN VARCHAR2,
                            p_st_time IN DATE,
                            p_end_time IN DATE,
                            p_mr_min IN NUMBER,
                            p_run_min IN NUMBER,
                            p_comp_num IN NUMBER,
                            p_task_num IN NUMBER,
                            p_psa_rsrc IN VARCHAR2,
                            p_psa_sts IN VARCHAR2,
                            p_qty IN NUMBER,
                            p_psa_strtime IN NUMBER,
                            p_psa_edtime IN NUMBER,
                            p_ptaskstartdt IN DATE,
                            p_ptaskenddt IN DATE,
                            p_rawmatl IN VARCHAR2,
                            p_prod_group IN VARCHAR,
                            p_job_type IN VARCHAR2,
                            p_time_required IN VARCHAR2,
                            p_psa_notes IN VARCHAR2);
                            
PROCEDURE bld_cap_rpt_dat(p_sts OUT VARCHAR2,
                          p_sts_msg OUT VARCHAR2);
                          
PROCEDURE clr_psa_task_det;

PROCEDURE upd_wrkday_cal(p_bldg IN VARCHAR2,
                          p_st_date IN DATE,
                          p_end_date IN DATE,
                          p_conv_lab_hrs IN NUMBER,
                          p_dat_mode IN VARCHAR2);

end mtag_efir_cap_rep_utls;
/


CREATE OR REPLACE package body      mtag_efir_cap_rep_utls AS

  -- Globals
  
  cap_rpt_rec               mastertag.mtag_efir_prod_cap_rpt_dat%ROWTYPE;
  
-- Procedure to complete and write report detail record
-- Locates workday record
-- Consumes capacity
-- Reports unconsumed task hours
PROCEDURE task_hrs_consum(p_task_hrs IN NUMBER,
                          p_task_date IN DATE,
                          p_start_time IN DATE,
                          p_end_time IN DATE,
                          p_task_comp IN VARCHAR2,
                          p_task_cont IN VARCHAR2,
                          p_task_loc IN VARCHAR2,
                          p_hrs_consum OUT NUMBER,
                          p_act_date OUT DATE,
                          p_sts OUT VARCHAR2) AS
    
    /*                      
    p_task_hrs        Remaining task hours
    p_task_date       Date to try for (or next workday)
    p_start_time      Start time for task
    p_end_time        End time - if task is being placed complete
    p_task_comp       Task fits in day (per PSA) - just place it
    p_task_cont       Continues from previous date - start time is beginning of shift
    p_task_loc        Location (WALSH or FC)
    p_hrs_consum      Hours consumed
    p_act_date        Actual date where task placed
    p_sts             Status (SUCCESS or FAIL)
    */
    
  CURSOR c_wrkday_cal IS
    SELECT *
    FROM  mastertag.mtag_efir_prod_wrkday_cal
    WHERE TRUNC(prod_date) >= TRUNC(p_task_date)
    AND   prod_location = p_task_loc
    ORDER BY prod_date;
    
  cal_rec                   c_wrkday_cal%ROWTYPE;
    
  v_sts                     VARCHAR2(10);
  v_hrs_cons                NUMBER;         -- Hours consumed
  v_hrs_avail               NUMBER;         -- Hours available in shift - after task start time
  
  v_wrk_time                 DATE;
                          
    
BEGIN

  v_sts := 'SUCCESS';

  -- get workday record
  OPEN c_wrkday_cal;
  
  FETCH c_wrkday_cal
  INTO  cal_rec;
  
  IF c_wrkday_cal%FOUND
    THEN
      -- workday record located
      -- Fill in capacity numbers for day
      cap_rpt_rec.wc_avl_hrs := cal_rec.wc_hrs_avail;
      cap_rpt_rec.lab_avl_hrs := cal_rec.conv_lab_hrs_avail;
      cap_rpt_rec.task_date := cal_rec.prod_date;
      IF p_task_comp = 'Y'      -- place task in this day
        THEN
          v_hrs_cons := p_task_hrs;
          cap_rpt_rec.wc_hrs_curr_day := p_task_hrs;
          cap_rpt_rec.task_start := p_start_time;
          cap_rpt_rec.task_end := p_end_time;
      ELSE
          -- Try to fit task
          -- time available is remainder of shift
          v_wrk_time := TO_DATE(TO_CHAR(cal_rec.shift_end, 'DD-MON-YYYY') || ' ' || TO_CHAR(p_start_time, 'HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS');
          v_hrs_avail := (cal_rec.shift_end - TO_DATE(TO_CHAR(cal_rec.shift_end, 'DD-MON-YYYY') || ' ' || TO_CHAR(p_start_time, 'HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS')) * 24;
          
          IF p_start_time IS NOT NULL
            THEN
              cap_rpt_rec.task_start := p_start_time;
          ELSE
              cap_rpt_rec.task_start := cal_rec.shift_start;
          END IF;
          
          IF v_hrs_avail >= p_task_hrs
            THEN
              -- Will fit in current day
              v_hrs_cons := p_task_hrs;
              
              cap_rpt_rec.task_end := cap_rpt_rec.task_start + (p_task_hrs / 24);
              cap_rpt_rec.wc_hrs_curr_day := p_task_hrs;
          ELSE
              -- Will not fit in current day
              v_hrs_cons := v_hrs_avail;
              cap_rpt_rec.task_end := cal_rec.shift_end;
              cap_rpt_rec.wc_hrs_curr_day := v_hrs_avail;
          END IF;
      END IF;
      
      -- Complete hours calculation
      cap_rpt_rec.lab_hrs_curr_day := cap_rpt_rec.wc_hrs_curr_day * cap_rpt_rec.crew_size;
      cap_rpt_rec.wc_util_pct := cap_rpt_rec.wc_hrs_curr_day / cap_rpt_rec.wc_avl_hrs;
      
      IF cap_rpt_rec.lab_avl_hrs > 0
        THEN
          cap_rpt_rec.lab_util_pct := cap_rpt_rec.lab_hrs_curr_day / cap_rpt_rec.lab_avl_hrs;
      ELSE
          cap_rpt_rec.lab_util_pct := 0;
      END IF;
      
      -- Write to table
      INSERT INTO mastertag.mtag_efir_prod_cap_rpt_dat
        VALUES cap_rpt_rec;
        
      COMMIT;
      
      v_sts := 'SUCCESS';
  ELSE
      v_sts := 'FAIL';
  END IF;
  
  CLOSE c_wrkday_cal;
  
  -- Load parameters for return
  p_sts := v_sts;
  p_act_date := cap_rpt_rec.task_start;
  p_hrs_consum := cap_rpt_rec.wc_hrs_curr_day;
  
END;

-- Procedure to write task error record
PROCEDURE wrt_task_err(p_err_msg IN VARCHAR2) AS

BEGIN

   cap_rpt_rec.wc_avl_hrs := 0;
   cap_rpt_rec.lab_avl_hrs := 0;
  
   cap_rpt_rec.wc_hrs_curr_day := 0;
   
   cap_rpt_rec.lab_hrs_curr_day := 0;
   cap_rpt_rec.wc_util_pct := 0;

   cap_rpt_rec.lab_util_pct := 0;
   
   cap_rpt_rec.comments := p_err_msg;
   
   INSERT INTO mastertag.mtag_efir_prod_cap_rpt_dat
    VALUES cap_rpt_rec;
    
END;

-- Get bundle size(s) for job
-- Returns unique bundles size string for a job
FUNCTION rtrv_job_bndl_siz(p_job VARCHAR2) RETURN VARCHAR2 AS

  v_retval                      mastertag.mtag_efir_prod_cap_rpt_dat.bundle_size%TYPE;
  
 /* CURSOR c_bs IS
    SELECT DISTINCT anl."AnalysisCode" AS b_size
    FROM  pub.pv_itemanalysiscode@vision anl,
          pub.pv_jobline@vision jl
    WHERE anl."CompNum" = jl."CompNum"
    AND   anl."ItemCode" = jl."ItemCode"
    AND   "AnalysisType" = '2 Bundle Size'
    AND   jl."JobCode" = p_job; */
    
  CURSOR c_bs IS
  SELECT DISTINCT summ.bundle_size as b_size
  FROM    efi_stage.mtag_efir_hrd_item_summ summ,
          dbadmin.job_positions jpos
  WHERE   jpos.demand_item_code = summ.item_number
  AND     jpos.jobnum = p_job;
    
  v_cnt                 PLS_INTEGER;
  
BEGIN
  
  v_retval := NULL;
  
  SELECT COUNT(*)
  INTO  v_cnt
  FROM pub.pv_jobline@vision
  WHERE "JobCode" = p_job;
  
  IF NVL(v_cnt, 0) > 0
    THEN
  
      -- Pass bundle sizes and assemble output string
      FOR bs_rec IN c_bs
      LOOP
      
        IF v_retval IS NULL
          THEN
            v_retval := bs_rec.b_size;
        ELSE
            v_retval := v_retval || ' / ' || bs_rec.b_size;
        END IF;
        
      END LOOP;
  
  END IF;
  
  -- Return bundle size string
  RETURN v_retval;
  
/*EXCEPTION
WHEN OTHERS THEN
  v_retval := NULL;
  RETURN v_retval; */
  
END;

-- Procedure to update capacity detail with job bundle sizes
PROCEDURE set_job_bndl_siz AS

  CURSOR c_job IS
    SELECT DISTINCT job_number
    FROM  mastertag.mtag_efir_prod_cap_rpt_dat;
    
  v_bndl_siz                  mastertag.mtag_efir_prod_cap_rpt_dat.bundle_size%TYPE;
    
BEGIN

  -- Pass through unique job values
  FOR job_rec IN c_job
  LOOP
  
    -- Get bundle size string for job
    v_bndl_siz := rtrv_job_bndl_siz(job_rec.job_number);
    
    -- Update job data
    IF v_bndl_siz IS NOT NULL
      THEN
        UPDATE mastertag.mtag_efir_prod_cap_rpt_dat
        SET     bundle_size = v_bndl_siz
        WHERE   job_number = job_rec.job_number;
    END IF;
    
    END LOOP;
    
    COMMIT;
    
END;


-- Procedure to build capacity report table
-- Reads extract from PSA and generates report detail for each task
-- Resolves tasks that span days
-- Calculates work center and labor capacity absorption
-- Called externally from .Net program
PROCEDURE bld_cap_rpt_dat(p_sts OUT VARCHAR2,
                          p_sts_msg OUT VARCHAR2) AS

  CURSOR c_psa_dat IS
    SELECT *
    FROM    mastertag.mtag_efir_prod_job_task_det
    WHERE   status = 'P'          -- Planned jobs only
    AND     psa_resource != 'parked'
    AND     (NVL(mr_time_min, 0) + NVL(run_time_min, 0)) != 0
    ORDER BY psa_strtime;
    
  v_task_start            DATE;
  v_prod_loc              mastertag.mtag_efir_prod_wrkday_cal.prod_location%TYPE;
    
  CURSOR c_wrkday_strt IS
    SELECT *
    FROM  mastertag.mtag_efir_prod_wrkday_cal
    WHERE prod_location = v_prod_loc
    AND   shift_start <= v_task_start
    AND   shift_end >= v_task_start
    ORDER BY prod_date;
    
  v_prod_date             DATE;
    
  CURSOR c_wrkdays IS
    SELECT *
    FROM  mastertag.mtag_efir_prod_wrkday_cal
    WHERE TRUNC(prod_date) >= TRUNC(v_prod_date)
    AND   prod_location = v_prod_loc
    ORDER BY prod_date;
    
  wdy_rec               mastertag.mtag_efir_prod_wrkday_cal%ROWTYPE;

  v_sts                 VARCHAR2(10);
  v_sts_msg             VARCHAR2(100);
  v_task_hrs            NUMBER;
  v_hrs_consum          NUMBER;
  v_act_date            DATE;
  v_consum_sts          VARCHAR2(20);
  v_start_time          DATE;
  v_task_date           DATE;
  v_run_len             NUMBER;
  v_cmnts               VARCHAR2(1000);
  v_compnum             NUMBER;
  v_prod_est            VARCHAR2(20);
  v_hrs_remn            NUMBER;
  v_first_day_flag      PLS_INTEGER;
  v_day_hrs_avail       NUMBER;
  v_end_time            DATE;
  
  v_min_wrk             VARCHAR2(10);
  
  wc_rec                mastertag.mtag_efir_prod_wc_mst%ROWTYPE;

  -- CR#5054 Capacity Report Performance update
  -- add the lv_compnum variable
  lv_compnum number := efi_stage.mtag_staging_support.get_company_number;

BEGIN

  -- Clear out reporting table
  DELETE FROM mastertag.mtag_efir_prod_cap_rpt_dat;
  
  COMMIT;
  
  v_sts := 'SUCCESS';        -- Default to success return status
  
  -- Pass task detail
  FOR task_rec IN c_psa_dat
  LOOP
      -- Get work center record
      BEGIN
      SELECT *
      INTO  wc_rec
      FROM  mastertag.mtag_efir_prod_wc_mst
      WHERE wc_code = task_rec.psa_resource;
      
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- Handle Maintenance Jobs
        IF task_rec.psa_resource = 'parked'
          THEN
            wc_rec := NULL;
            wc_rec.wc_code := 'parked';
            wc_rec.wc_name := task_rec.work_center;
            wc_rec.crew_size := 0;
            wc_rec.wc_location := 'WALSH';
            wc_rec.wc_op_type := 'MAINT';
            wc_rec.wc_op := 'Maint';
        ELSE
            -- work center not found
            cap_rpt_rec := NULL;
      
            cap_rpt_rec.job_number := task_rec.job_number;
            cap_rpt_rec.wc_code := task_rec.psa_resource;

            cap_rpt_rec.prod_group := task_rec.prod_group;
            cap_rpt_rec.raw_matl := task_rec.rawmatl;
            cap_rpt_rec.mr_time_hrs_task := ROUND(task_rec.mr_time_min / 60, 2);
            cap_rpt_rec.run_time_hrs_task := ROUND(task_rec.run_time_min / 60, 2);
            
            cap_rpt_rec.job_type_t_l := task_rec.job_type_t_l;
            cap_rpt_rec.time_required := task_rec.time_required;
            cap_rpt_rec.psa_notes := task_rec.psa_notes;
            
            cap_rpt_rec.task_date := task_rec.start_time;
            cap_rpt_rec.task_start := task_rec.start_time;
            cap_rpt_rec.task_end := task_rec.end_time;
            
            wrt_task_err(' *** Work center - ' || task_rec.psa_resource || ' - not found ***');
            
            v_sts := 'FAIL';
            
          GOTO loop_ext;
        END IF;
      END;
      
      -- Get run length
      BEGIN
      SELECT ed."estdiech-qty",
              jb."CompNum",
              jb."EstCode"
      INTO  v_run_len,
            v_compnum,
            v_prod_est
      FROM  pub.estqtydiechanges@vision ed,
            pub.pv_job@vision jb
      WHERE jb."JobCode" = task_rec.job_number
  -- CR#5054 Capacity Report Performance update
  -- use lv_compnum in the query to fetch run length
      AND jb."CompNum" = lv_compnum
      AND   ed."k-est-code" = jb."EstCode"
      AND   ed."kco" = jb."CompNum";
      
      EXCEPTION
      WHEN OTHERS THEN
        v_run_len := 0;
        v_prod_est := NULL;
      END;
      
      -- Get Job Comments
      IF v_prod_est IS NOT NULL
        THEN
          BEGIN
          SELECT apps.mtag_web_reports.get_prod_est_narrative(v_compnum, v_prod_est)
          INTO  v_cmnts
          FROM dual;
          
          EXCEPTION
          WHEN OTHERS THEN
            v_cmnts := NULL;
          END;
      ELSE
          v_cmnts := NULL;
      END IF;
      
      -- Initialize task output record
      cap_rpt_rec := NULL;
      
      -- Populate common columns
      cap_rpt_rec.job_number := task_rec.job_number;
      cap_rpt_rec.wc_code := task_rec.psa_resource;
      cap_rpt_rec.wc_name := wc_rec.wc_name;
      cap_rpt_rec.wc_location := wc_rec.wc_location;
      cap_rpt_rec.prod_group := task_rec.prod_group;
      cap_rpt_rec.raw_matl := task_rec.rawmatl;
      cap_rpt_rec.mr_time_hrs_task := ROUND(task_rec.mr_time_min / 60, 2);
      cap_rpt_rec.run_time_hrs_task := ROUND(task_rec.run_time_min / 60, 2);
      cap_rpt_rec.wc_op_type := wc_rec.wc_op_type;
      cap_rpt_rec.wc_op := wc_rec.wc_op;
      cap_rpt_rec.wc_op_type := wc_rec.wc_op_type;
      cap_rpt_rec.crew_size := wc_rec.crew_size;
      --cap_rpt_rec.run_length := v_run_len;
      cap_rpt_rec.run_length := FLOOR(task_rec.psa_qty);    -- Use PSA quantity
      cap_rpt_rec.comments := v_cmnts;
      
      cap_rpt_rec.job_type_t_l := task_rec.job_type_t_l;
      cap_rpt_rec.time_required := task_rec.time_required;
      cap_rpt_rec.psa_notes := task_rec.psa_notes;
      
      v_min_wrk := TO_CHAR(ROUND(MOD((task_rec.mr_time_min + task_rec.run_time_min), 60), 2));
      
      IF LENGTH(v_min_wrk) < 2
        THEN
          v_min_wrk := '0' || v_min_wrk;
      END IF;
      
      cap_rpt_rec.task_time_hr_min := TO_CHAR(FLOOR(cap_rpt_rec.mr_time_hrs_task + cap_rpt_rec.run_time_hrs_task)) || ':' ||
                                  v_min_wrk;
      
      -- Task hours to consume
      v_task_hrs := cap_rpt_rec.mr_time_hrs_task + cap_rpt_rec.run_time_hrs_task;
      
      -- Determine start prod date
      v_task_start := task_rec.start_time;
      v_prod_loc := wc_rec.wc_location;
      v_hrs_remn := v_task_hrs;
      
      if c_wrkday_strt%isopen then
        close c_wrkday_strt;
      end if;
      
      OPEN c_wrkday_strt;
      
      FETCH c_wrkday_strt
      INTO  wdy_rec;
      
      IF c_wrkday_strt%FOUND
        THEN
          v_first_day_flag := 5;        -- This is first day for task
          v_prod_date := wdy_rec.prod_date;
          
          -- Start date found - start consuming hours
          WHILE v_hrs_remn > 0
          LOOP
            -- Set start time in day
            IF v_first_day_flag > 0     -- First day
              THEN
                v_start_time := v_task_start;        -- Actusl start time
            ELSE
                v_start_time := wdy_rec.shift_start;    -- continuation day - use shift start time
            END IF;
            
            -- Determine nours left in day
            v_day_hrs_avail := (wdy_rec.shift_end - v_start_time) * 24;
            
            -- Determine hours to use
            IF v_day_hrs_avail >= v_hrs_remn
              THEN
                v_hrs_consum := v_hrs_remn;         -- Remaining hours will fit
                v_end_time := v_start_time + (v_hrs_consum / 24);
                v_hrs_remn := 0;
            ELSE
                v_hrs_consum := v_day_hrs_avail;    -- Use what is left in day
                v_end_time := wdy_rec.shift_end;
                v_hrs_remn := v_hrs_remn - v_hrs_consum;
                
            END IF;
            
            -- Fill in fields in capacity report record
             cap_rpt_rec.wc_avl_hrs := wdy_rec.wc_hrs_avail;
             cap_rpt_rec.lab_avl_hrs := wdy_rec.conv_lab_hrs_avail;
             cap_rpt_rec.task_date := wdy_rec.prod_date;
             
             cap_rpt_rec.wc_hrs_curr_day := v_hrs_consum;
             cap_rpt_rec.task_start := v_start_time;
             cap_rpt_rec.task_end := v_end_time;
             
             cap_rpt_rec.lab_hrs_curr_day := cap_rpt_rec.wc_hrs_curr_day * cap_rpt_rec.crew_size;
             cap_rpt_rec.wc_util_pct := cap_rpt_rec.wc_hrs_curr_day / cap_rpt_rec.wc_avl_hrs;
      
             IF cap_rpt_rec.lab_avl_hrs > 0
             THEN
                  cap_rpt_rec.lab_util_pct := cap_rpt_rec.lab_hrs_curr_day / cap_rpt_rec.lab_avl_hrs;
             ELSE
                  cap_rpt_rec.lab_util_pct := 0;
             END IF;
             
             -- Set multiple day flag if necessary
             IF v_hrs_remn > 0
              THEN
                cap_rpt_rec.mult_day_task := 'Y';
            END IF;
            
            -- Write to table
            INSERT INTO mastertag.mtag_efir_prod_cap_rpt_dat
              VALUES cap_rpt_rec;
              
            -- Clear first day flag
            IF v_first_day_flag > 0
              THEN
                v_first_day_flag := 0;
                CLOSE c_wrkday_strt;
            ELSE
                CLOSE c_wrkdays;
            END IF;
            
            -- If more hours to consume - get next workday record
            IF v_hrs_remn > 0
              THEN
                v_prod_date := v_prod_date + 1;
                
                OPEN c_wrkdays;
                FETCH c_wrkdays
                INTO  wdy_rec;

                -- CAM 20171109 set v_prod_date = the date just fetched
                -- to prevent weekends from causing repeated work days
                if wdy_rec.prod_date is not null then
                  v_prod_date := wdy_rec.prod_date;
                end if;
                --
                
                  IF c_wrkdays%NOTFOUND
                    THEN
                      cap_rpt_rec.task_date := v_prod_date;
            
                      wrt_task_err(' *** Cannot find next production date for ' || v_prod_date || ' ***');
                      v_sts := 'FAIL';
                      
                      CLOSE c_wrkdays;
                      
                      GOTO loop_ext;
                  END IF;
                
            END IF;
             
          END LOOP;
          
      ELSE
          CLOSE c_wrkday_strt;
          -- Cannot find start date
          cap_rpt_rec.task_date := task_rec.start_time;
          cap_rpt_rec.task_start := task_rec.start_time;
          cap_rpt_rec.task_end := task_rec.end_time;
            
          wrt_task_err(' *** Cannot locate production date ***');
          
          v_sts := 'FAIL';
          
          GOTO loop_ext;
      
      END IF;
      
    <<loop_ext>>
    
    COMMIT;
      
     
  
  END LOOP;
  
  -- Load in bundle size
  -- set_job_bndl_siz;
  
  IF v_sts = 'FAIL'
    THEN
      v_sts_msg := 'Refresh completed with errors';
      v_sts := 'SUCCESS';
  END IF;
  
  <<proc_ext>>
  
  -- Load parameters for return
  p_sts := v_sts;
  p_sts_msg := v_sts_msg;
  
END;

PROCEDURE lod_psa_task_dat(p_job IN VARCHAR2,
                            p_wc IN VARCHAR2,
                            p_st_time IN DATE,
                            p_end_time IN DATE,
                            p_mr_min IN NUMBER,
                            p_run_min IN NUMBER,
                            p_comp_num IN NUMBER,
                            p_task_num IN NUMBER,
                            p_psa_rsrc IN VARCHAR2,
                            p_psa_sts IN VARCHAR2,
                            p_qty IN NUMBER,
                            p_psa_strtime IN NUMBER,
                            p_psa_edtime IN NUMBER,
                            p_ptaskstartdt IN DATE,
                            p_ptaskenddt IN DATE,
                            p_rawmatl IN VARCHAR2,
                            p_prod_group IN VARCHAR,
                            p_job_type IN VARCHAR2,
                            p_time_required IN VARCHAR2,
                            p_psa_notes IN VARCHAR2) AS

    task_rec                mastertag.mtag_efir_prod_job_task_det%ROWTYPE;
    
BEGIN
    
    task_rec := NULL;
    
    task_rec.job_number := p_job;
    task_rec.work_center := p_wc;
    task_rec.start_time := p_st_time;
    task_rec.end_time := p_end_time;
    task_rec.mr_time_min := p_mr_min;
    task_rec.run_time_min := p_run_min;
    task_rec.comp_num := p_comp_num;
    task_rec.task_num := p_task_num;
    task_rec.psa_resource := p_psa_rsrc;
    task_rec.status := p_psa_sts;
    task_rec.psa_qty := p_qty;
    task_rec.psa_strtime := p_psa_strtime;
    task_rec.psa_edtime := p_psa_edtime;
    task_rec.psa_ptaskstartdt := p_ptaskstartdt;
    task_rec.psa_ptaskenddt := p_ptaskenddt;
    task_rec.rawmatl := p_rawmatl;
    task_rec.prod_group := p_prod_group;
    task_rec.job_type_t_l := p_job_type;
    task_rec.time_required := p_time_required;
    task_rec.psa_notes := p_psa_notes;
    
    INSERT INTO mastertag.mtag_efir_prod_job_task_det
        VALUES task_rec;
        
    COMMIT;
    
END;

-- Procedure to update workday calendar
-- Deletes existing entries in date range
-- Adds new dates
-- Input parameter determines whether to include only workdays
PROCEDURE upd_wrkday_cal(p_bldg IN VARCHAR2,
                          p_st_date IN DATE,
                          p_end_date IN DATE,
                          p_conv_lab_hrs IN NUMBER,
                          p_dat_mode IN VARCHAR2) AS
                          
  /*
    p_bldg            Building (WALSH or FC)
    p_st_date         Start date and time
    p_end_date        End date and time
    p_conv_lab_hrs    Converting labor hours
    p_dat_mode        Workdays only?
                        W - working days only (ignore weekends and holidays)
                        A - Use all days in range
  */
                          
  v_wrk_dat           DATE;
  v_shift_start       VARCHAR2(20);   -- Shift start time
  v_shift_end         VARCHAR2(20);   -- Shift end time
  
  wdy_rec             mastertag.mtag_efir_prod_wrkday_cal%ROWTYPE;
  
BEGIN

  v_wrk_dat := TRUNC(p_st_date);
  
  v_shift_start := TO_CHAR(p_st_date, 'HH24:MI:SS');
  v_shift_end := TO_CHAR(p_end_date, 'HH24:MI:SS');
  
  WHILE v_wrk_dat <= TRUNC(p_end_date)
  LOOP
    
    IF p_dat_mode = 'A'     -- All days in range
      OR  (RTRIM(TO_CHAR(v_wrk_dat, 'DAY')) NOT IN ('SATURDAY', 'SUNDAY')
          AND TO_CHAR(v_wrk_dat, 'DD-Mon-YYYY') NOT IN ('07-Sep-2015', '26-Nov-2015', '27-Nov-2015',
                                                        '24-Dec-2015', '25-Dec-2015', '31-Dec-2015',
                                                        '01-Jan-2016')
          AND TO_CHAR(v_wrk_dat, 'DD-Mon-YYYY') NOT IN ('30-May-2016', '04-Jul-2016', '05-Sep-2016',
                                                        '24-Nov-2016', '25-Nov-2016', '23-Dec-2016',
                                                        '26-Dec-2016', '30-Dec-2016', '02-Jan-2017')
          AND TO_CHAR(v_wrk_dat, 'DD-Mon-YYYY') NOT IN ('29-May-2017', '04-Jul-2017', '04-Sep-2017',
                                                       '23-Nov-2017', '24-Nov-2017', '25-Dec-2017',
                                                       '26-Dec-2017', '01-Jan-2017', '02-Jan-2017'))
        THEN
          -- Add workday record
          wdy_rec := NULL;
          
          wdy_rec.prod_date := TO_DATE(TO_CHAR(v_wrk_dat, 'DD-MON-YYYY'), 'DD-MON-YYYY');
          wdy_rec.day_type := 'W';
          
          IF v_shift_start > v_shift_end
            THEN
              -- Shift begins prior day
              wdy_rec.shift_start := TO_DATE(TO_CHAR(v_wrk_dat - 1, 'DD-MON-YYYY') || ' ' || v_shift_start, 'DD-MON-YYYY HH24:MI:SS');
          ELSE
              wdy_rec.shift_start := TO_DATE(TO_CHAR(v_wrk_dat, 'DD-MON-YYYY') || ' ' || TO_CHAR(p_st_date, 'HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS');
          END IF;
          
          wdy_rec.shift_end := TO_DATE(TO_CHAR(v_wrk_dat, 'DD-MON-YYYY') || ' ' || TO_CHAR(p_end_date, 'HH24:MI:SS'), 'DD-MON-YYYY HH24:MI:SS');
          wdy_rec.conv_lab_hrs_avail := p_conv_lab_hrs;
          wdy_rec.prod_location := p_bldg;
          wdy_rec.wc_hrs_avail := FLOOR((wdy_rec.shift_end - wdy_rec.shift_start) * 24);
          
          DBMS_OUTPUT.PUT_LINE('Inserting: ' || wdy_rec.prod_date);
          
          -- Delete old rec - if any
          DELETE FROM mastertag.mtag_efir_prod_wrkday_cal
            WHERE TRUNC(prod_date) = TRUNC(wdy_rec.prod_date)
            AND   prod_location = wdy_rec.prod_location;
          
          COMMIT;
          
          INSERT INTO mastertag.mtag_efir_prod_wrkday_cal
            VALUES wdy_rec;
            
          COMMIT;
      END IF;
          
          v_wrk_dat := v_wrk_dat + 1;
  
  
  END LOOP;

  
END;

-- Procedure to clear out PSA task detail table
PROCEDURE clr_psa_task_det AS

BEGIN

  DELETE FROM mastertag.mtag_efir_prod_job_task_det;
  
  COMMIT;
  
END;


end mtag_efir_cap_rep_utls;
/
